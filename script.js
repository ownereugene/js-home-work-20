const vehicles = [
  {
    name: "Toyota",
    model: "Crown",
    year: 2019,
    color: "Red",
    contentType: { name: "en_US", type: "jstring" },
    locales: [
      { name: "en_US", description: "English" },
      {name: "de_Ger", description: "Germania" },
      
    ],
  },

  {
    name: "BMW",
    model: "X5",
    year: 2023,
    color: "White",
    contentType: { name: "json", type: "jstring" },
    locales: [
      { name: "en_US", description: "English" },
      { name: "de_Ger", description: "Germania" },
      { name: "ukr_UA", description: "Ukraine" },
    ],
  },

  {
    name: "Audi",
    model: "RS6",
    year: 2021,
    color: "Red",
    contentType: { name: "json", type: "jstring" },
    locales: [
      { name: "en_US", description: "English" },
      { name: "de_Ger", description: "Germania" },
      { name: "ukr_UA", description: "Ukraine" },
    ],
  },
];

function filterCollection(arr, keyWords, bool, ...rest) {
  let keyWordsArr = keyWords.split(" ");
  let pass = rest;

  const filterArr = arr.filter((element) => {
    
    let restKeyWords = [];

    function innerArr(data) {
      Object.values(data).forEach((el) => {
        if (Array.isArray(el)) {
          el.forEach((elem) => innerArr(elem));
        }
      });

      for (let val of pass) {
        if (Object.keys(data).includes(val)) {
          for (let keyWord of keyWordsArr) {
            if (Object.values(data).includes(keyWord)) {
              if (!restKeyWords.includes(keyWord)) {
                restKeyWords.push(keyWord);
              }
            }
          }
        }
      }
    }
    innerArr(element);
    return bool
      ? restKeyWords.length === keyWordsArr.length
      : restKeyWords.length > 0;
  });

  return filterArr;
}



console.table(
  filterCollection(
    vehicles,
    "Ukraine",
    true,
    "name",
    "model",
    "color",
    "description",
    "contentType",
    "locales.description",
    "locales.name"
  )
);
